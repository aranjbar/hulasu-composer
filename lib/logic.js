/**
 * Add new goods to hulasu
 * @param {org.goodsnet.AddGoods} tx - the transaction
 * @transaction
 */
async function addGoods(tx) {
  const factory = getFactory();
  const NS = "org.goodsnet";
  const { goodsName, goodsPrice, goodsAmount, shopper } = tx;
  const goodsKey = makeKey([goodsName, goodsPrice]);
  const assetRegistry = await getAssetRegistry(NS + ".Goods");
  const participantRegistry = await getParticipantRegistry(NS + ".User");
  const now = getNow();
  if (goodsAmount < 1) {
    throw new Error("The amount value must be greater than 0.");
  }
  if (goodsPrice < 0) {
    throw new Error("The goods price cannot be negative.");
  }
  if (goodsName === "") {
    throw new Error("The goods name cannot be empty.");
  }
  try {
    const goods = await assetRegistry.get(goodsKey);
    goods.goodsAmount += goodsAmount;
    goods.updatedAt = now;
    await assetRegistry.update(goods);
  } catch (e) {
    const newGoods = factory.newResource(NS, "Goods", goodsKey);
    newGoods.goodsName = goodsName;
    newGoods.goodsPrice = goodsPrice;
    newGoods.goodsAmount = goodsAmount;
    newGoods.createdAt = now;
    newGoods.updatedAt = now;
    await assetRegistry.add(newGoods);
  }
  shopper.balance += goodsAmount * goodsPrice;
  await participantRegistry.update(shopper);
}

/**
 * Buy some goods
 * @param {org.goodsnet.BuyGoods} tx - the transaction
 * @transaction
 */
async function buyGoods(tx) {
  const NS = "org.goodsnet";
  const { buyer, goods, goodsAmount } = tx;
  const assetRegistry = await getAssetRegistry(NS + ".Goods");
  const participantRegistry = await getParticipantRegistry(NS + ".User");
  const now = getNow();
  if (goodsAmount > goods.goodsAmount) {
    throw new Error("There isn't enough goods in shelf.");
  }
  try {
    // const goods = await assetRegistry.get(goods.getIdentifier());
    const { goodsPrice } = goods;
    buyer.balance -= goodsPrice * goodsAmount;
    buyer.updatedAt = now;
    goods.goodsAmount -= tx.goodsAmount;
    goods.updatedAt = now;
    if (goods.goodsAmount === 0) {
      await assetRegistry.remove(gooods);
    } else {
      await assetRegistry.update(goods);
    }
    await participantRegistry.update(buyer);
  } catch (e) {
    throw e;
  }
}

/**
 * Add new user
 * @param {org.goodsnet.AddUser} tx - the transaction
 * @transaction
 */
async function addUser(tx) {
  const NS = "org.goodsnet";
  const factory = getFactory();
  const participantRegistry = await getParticipantRegistry(NS + ".User");
  const { username } = tx;
  const newUser = factory.newResource(NS, "User", username);
  const now = getNow();
  newUser.balance = 0;
  newUser.createdAt = now;
  newUser.updatedAt = now;
  await participantRegistry.add(newUser);
}

/**
 * Charge user balance
 * @param {org.goodsnet.ChargeBalance} tx - the transaction
 * @transaction
 */
async function chargeBalance(tx) {
  const NS = "org.goodsnet";
  const participantRegistry = await getParticipantRegistry(NS + ".User");
  const { user, amount } = tx;
  try {
    user.balance += amount;
    user.updatedAt = getNow();
    await participantRegistry.update(user);
  } catch (e) {
    throw e;
  }
}

/**
 * Transfer money
 * @param {org.goodsnet.Transfer} tx - the transaction
 * @transaction
 */
async function transfer(tx) {
  const NS = "org.goodsnet";
  const participantRegistry = await getParticipantRegistry(NS + ".User");
  const { fromUser, toUser, amount } = tx;
  const now = getNow();
  try {
    fromUser.balance -= amount;
    fromUser.updatedAt = now;
    toUser.balance += amount;
    toUser.updatedAt = now;
    await participantRegistry.updateAll([fromUser, toUser]);
  } catch (e) {
    throw e;
  }
}

function makeKey(keyParts) {
  return keyParts.join(":");
}

function getNow() {
  const now = new Date()
    .toLocaleString("en-us", {
      weekday: "short",
      year: "numeric",
      month: "2-digit",
      day: "2-digit",
      hour: "2-digit",
      minute: "2-digit",
      second: "2-digit",
      hour12: false
    })
    .replace(
      /([a-zA-Z]+), (\d+)\/(\d+)\/(\d+), (\d+):(\d+):(\d+)/,
      "$1, $4-$2-$3, $5:$6:$7"
    );
  return now;
}
